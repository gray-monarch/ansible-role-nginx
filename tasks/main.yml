---
# tasks file for gray-monarch.nginx

- include_vars: "{{ item }}"
  with_first_found:
    - "{{ ansible_distribution }}.yml"
    - "{{ ansible_os_family }}.yml"

- include: "{{ item }}"
  with_first_found:
    - "t_{{ ansible_distribution }}.yml"
    - "t_{{ ansible_os_family }}.yml"

- name: Include NGINX repo tasks if 3rd party repo enabled
  include: nginx_repo.yml
  when: gm_nginx_include_3rd_party_repo|bool

- name: Install NGINX [with deps and upgrade on repo change]
  package:
    name: "{{ gm_nginx_install_pkgs }}"
    state: "{{ (installed_repo_result | changed) | ternary('latest','present') }}"

- name: Check if Nginx was compiled with the HTTP/2 module
  shell: nginx -V 2>&1 | grep -q 'with-http_v2_module'
  register: nginx_http2_module
  changed_when: false
  failed_when: false

- name: Gather the current Nginx version string
  shell: nginx -v 2>&1 | awk 'BEGIN{ FS="/" } { print $2 }'
  register: nginx_version_string
  changed_when: false
  failed_when: false

- name: Ensure NGINX config, include, log dir exists
  file:
    state: directory
    path: "{{ item.path }}"
    mode: "{{ item.mode | default('0755') }}"
  with_items:
    - path: "{{ gm_nginx_etc_dir }}"
    - path: "{{ gm_nginx_include_dir }}"
    - path: "{{ gm_nginx_log_dir }}"
      mode: "0750"

- name: Build dictionary of ec2_tag_FQDN domains
  set_fact:
    gm_nginx_site_dict: >-
      {% set temp_dict = {} -%}
      {% for site in [item] -%}
        {{ temp_dict.update({site:gm_nginx_default_site_dict}) }}
      {%- endfor -%}
      {{ temp_dict | combine(gm_nginx_site_dict|default({})) }}
  with_items:
   - "{{ ec2_tag_FQDN | default([]) }}"
   - "{{ gm_nginx_server_name | default([]) }}"

- name: Template NGINX include (ssl, php) configurations
  template:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
  notify: Reload Nginx
  with_items: "{{ gm_nginx_global_templates }}"
  register: global_template_results

- name: "Generate Diffie-Hellman PFS key: {{ gm_nginx_ssl_dhparam_size }} bits"
  command: >-
    openssl dhparam -out {{ gm_nginx_ssl_dhparam | quote }}
    {{ gm_nginx_ssl_dhparam_size }}
  args:
    creates: "{{ gm_nginx_ssl_dhparam }}"

# Begin Block
- block:

  - name: Template virtual host configurations (for each)
    include: vhost_setup.yml
    with_dict: "{{ gm_nginx_site_dict }}"
    loop_control:
        loop_var: site_dict

  - name: Remove unmanaged files
    include: remove_unmanaged.yml

  when:
    - gm_nginx_site_dict != {}
    - gm_nginx_setup_vhosts == true
# End block

- name: Ensure subdomain webroots have the correct SELinux contexts [NGINX|vhost|SELinux]
  command: restorecon -rv {{ gm_nginx_webroot_root | quote }}
  when: ansible_selinux.status | default(None) | lower == "enabled"
  changed_when: result.stdout_lines | select('match','restorecon reset.*') | list | length > 0
  register: result

- name: Ensure the NGINX service is started and enabled
  service:
    name: nginx
    state: started
    enabled: yes
